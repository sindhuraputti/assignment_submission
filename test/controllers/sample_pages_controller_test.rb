require 'test_helper'

class SamplePagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Home | Assignment Submission"

  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "help | Assignment Submission"

  end
 
end
