class SamplePagesController < ApplicationController
  def home
    if logged_in?
      @resume = current_user.resumes.build
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      @rfeed_items = current_user.rfeed()

    end
  end
  def login
  end
  def help
  end
end
